---
layout: handbook-page-toc
title: "PR.1.02 - Performance Management Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# PR.1.02 - Performance Management

## Control Statement

GitLab performs ongoing performance management through regularly recurring 1:1 meetings between managers and their direct reports. Performance and compensation reviews are performed annually.

## Context

The purpose of this control is to ensure managers and their direct reports are in ongoing, open conversation with one another to stay current with projects, tasks, roadblocks, and so on. This benefits both parties - particularly with GitLab being all-remote and asynchronous - by facilitating regular feedback, timely issue escalation, decision making, and work prioritization.

## Scope

This control applies to GitLab management and leadership.

## Ownership

Control Owner: 
* 'People Operations'

Process Owner: 
* People Operations


## Guidance

A process to evaluate the performance of team-members.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Performance Management control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/861).

Examples of evidence an auditor might request to satisfy this control:


*  Evidence of 1:1 meetings between managers and direct reports
*  Documentation of annual performance and compensation reviews

### Policy Reference

*  [Handbook/Leadership/1-1](https://about.gitlab.com/handbook/leadership/1-1/)
*  [Handbook/People Group/promotions and transfers](https://about.gitlab.com/handbook/people-group/promotions-transfers/)
*  [Handbook/People Group/Global Compensation](https://about.gitlab.com/handbook/people-group/global-compensation/)

## Framework Mapping

* SOC2 CC
  * CC1.3
