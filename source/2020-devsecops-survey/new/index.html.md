---
layout: markdown_page
title: "Take GitLab's 2020 DevSecOps survey"
description: "And enter to win prizes!"
suppress_header: true
twitter_image: '/images/tweets/2020-devsecops-survey.png'
---

# Take GitLab's 2020 Global DevSecOps Survey

<p>&nbsp;</p>

Take our survey below and enter to win an Apple iPad Pro, Apple AirPods Pro, a Sonos One smart speaker, or GitLab Merch! Once you complete the survey, you'll have more than once chance at gaining entries for winning one of our prizes.

<iframe src="https://gitlab.fra1.qualtrics.com/jfe/form/SV_3yBJ72xIvbi8YrH"></iframe>
<style type="text/css">
  .breadcrumb {
    display: none;
  }
  iframe {
    height: 60vh;
    margin: 50px auto;
    max-width: 1280px;
    min-height: 400px;
    width: 100%;
  }
</style>
